#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>
#include <chrono>
#include "json.hpp"

struct Data {
	bool loop;
	int rcode;
	std::ostream& out;
	std::istream& in;
	unsigned int indent;
	const char* indentChar;
};

void display_indent(struct Data& data) {
	for (unsigned int i = 0; i < data.indent; i++) {
		data.out << data.indentChar;
	}
}

void display_prompt(const std::string& label, struct Data& data) {
	display_indent(data);
	data.out << "  - ";
	Json::print_string(data.out, label);
	data.out << ": ";
}

void display_dir_entrance(const std::string& label, struct Data& data) {
	display_indent(data);
	data.out << "  - ";
	Json::print_string(data.out, label);
	data.out << " -" << std::endl;
}

std::ostream& display_message(struct Data& data) {
	data.indent++;
	display_indent(data);
	data.indent--;
	return data.out << "  - ";
}

void highlight_prompt_line(
	const std::string& label,
	const std::string& entry,
	unsigned int stateCode,
	struct Data& data
) {
	data.out << "\e[A";
	display_prompt(label, data);
	data.out << entry;
	switch (stateCode) {
		case 0: data.out << " \e[92m✓\e[0m"; break;
		case 1: data.out << " \e[91m✗\e[0m"; break;
		case 2: data.out << " \e[94m✓\e[0m"; break;
		case 3: data.out << " \e[93m✗\e[0m"; break;
	}
	data.out << std::endl;
}

void extract_settings(Json& json, bool& display, bool& shuffle) {
	if (json.is_object()) {
		for (auto& it : json.object()) {
			if (it.second.is_boolean() && it.first == "display") {
				display = it.second.boolean();
			}
			else if (it.second.is_boolean() && it.first == "shuffle") {
				shuffle = it.second.boolean();
			}
		}
	}
}

bool json_match_answer_model(const Json& json) {
	if (json.is_string()) {
		return true;
	}
	else if (json.is_array()) {
		for (const auto& it : json.array()) {
			if (!it.is_string()) {
				return false;
			}
		}
		return json.array().size() > 0;
	}
	else {
		return false;
	}
}

bool is_a_valid_answer(const Json& store, const std::string& user) {
	if (store.is_string()) {
		return store.string() == user;
	}
	else if (store.is_array()) {
		for (const auto& it : store.array()) {
			if (it.is_string() && it.string() == user) {
				return true;
			}
		}
	}
	return false;
}

const std::string& get_answer_from(const Json& answer) {
	if (answer.is_string()) {
		return answer.string();
	}
	else if (answer.is_array() && answer.array().size() > 0) {
		return answer.array()[0].string();
	}
	const static std::string empty("");
	return empty;
}

void print_tree(const Json::Object& node, struct Data& data) {
	data.indent++;
	for (const auto& it : node) {
		if (it.first != "#") {
			display_indent(data);
			Json::print_string(data.out, it.first);
			data.out << ':';
			if (it.second.is_object()) {
				data.out << std::endl;
				print_tree(it.second.object(), data);
			}
			else {
				data.out << ' ';
				it.second.print(data.out);
				data.out << std::endl;
			}
		}
	}
	data.indent--;
}

void recursive_explore(Json& node, struct Data& data) {
	static std::string entry;
	bool optionDisplay = false;
	bool optionShuffle = false;

	auto iterator = node.object().begin();
	auto end = node.object().end();
	while (iterator != end) {
		if (iterator->second.is_object()) {
			if (iterator->first != "#") {
				display_dir_entrance(iterator->first, data);
				data.indent++;
				recursive_explore(iterator->second, data);
				data.indent--;
				if (not data.loop) return;
				iterator++;
			}
			else {
				extract_settings(
					iterator->second,
					optionDisplay,
					optionShuffle
				);
				iterator++;
				if (optionShuffle) {
					auto now = std::chrono::system_clock::now();
					unsigned int seed = now.time_since_epoch().count();
					std::shuffle(
						iterator, end, std::default_random_engine(seed)
					);
				}
			}
		}
		else if (json_match_answer_model(iterator->second)) {
			display_prompt(iterator->first, data);
			std::getline(data.in, entry);

			if (entry.size() > 0) {
				if (entry[0] == '/') {
					if (entry == "/exit") {
						data.loop = false;
						highlight_prompt_line(iterator->first, entry, 2, data);
						return;
					}
					else if (entry == "/tell") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						iterator->second.print(display_message(data));
						data.out << std::endl;
					}
					else if (entry == "/skip") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						iterator++;
					}
					else if (entry == "/skip all") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						return;
					}
					else if (entry == "/clear") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						data.out << "\e[2J\e[H" << std::flush;
					}
					else if (entry == "/list") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						display_message(data) << "/clear  clear screen"
						<< std::endl;
						display_message(data) << "/exit   exit the program"
						<< std::endl;
						display_message(data) << "/list   display command list"
						<< std::endl;
						display_message(data) << "/skip   skip current prompt"
						<< std::endl;
						display_message(data) << "/tell   give the answer"
						<< std::endl;
						display_message(data)
						<< "/tree   unfold the current node" << std::endl;
					}
					else if (entry == "/tree") {
						highlight_prompt_line(iterator->first, entry, 2, data);
						data.indent++;
						print_tree(node.object(), data);
						data.indent--;
					}
					else {
						highlight_prompt_line(iterator->first, entry, 3, data);
						display_message(data) << "command not found"
						<< std::endl;
					}
				}
				else {
					if (is_a_valid_answer(iterator->second, entry)) {
						highlight_prompt_line(
							iterator->first, entry, 0, data
						);
						iterator++;
					}
					else {
						highlight_prompt_line(
							iterator->first, entry, 1, data
						);
					}
				}
			}
			else if (optionDisplay) {
				highlight_prompt_line(
					iterator->first, get_answer_from(iterator->second), -1, data
				);
				iterator++;
			}
		}
		else {
			std::cerr << "invalid json value type, a string, an array of string or an object is expected"
			<< std::endl;
			data.rcode = EXIT_FAILURE;
			data.loop = false;
			return;
		}
	}
}

void search_entry(
	const Json& node,
	const std::string& entry,
	struct Data& data
) {
	if (not node.is_object()) {
		std::cerr << "invalid value type, an object is expected" << std::endl;
		data.rcode = EXIT_FAILURE;
		return;
	}
	for (
		Json::Object::const_iterator iterator = node.object().begin();
		iterator != node.object().end();
		iterator++
	) {
		if (iterator->first == entry) {
			data.out << iterator->second << std::endl;
			return;
		}
	}
	std::cerr << "no value found for entry ";
	Json::print_string(std::cerr, entry);
	std::cerr << std::endl;
	data.rcode = EXIT_FAILURE;
}

void display_usage(std::ostream& out, const char* arg0) {
	out << "usage: " << arg0 << " [options] <dictionary.json>" << std::endl;
	out << "options:" << std::endl;
	out << " -h, --help          display help" << std::endl;
	out << " -s, --search ENTRY  search for a specific entry" << std::endl;
	out << " -v, --version       display version" << std::endl;
}

int main(int argc, const char* argv[]) {

	enum Mode {
		MODE_NORMAL,
		MODE_SEARCH
	};
	enum Mode mode = MODE_NORMAL;

	const char* fileName = nullptr;
	const char* entry = nullptr;

	for (unsigned int i = 1; i < (unsigned int)argc; i++) {
		if (argv[i][0] == '-') {
			if (
				argv[i] == std::string("-h") or
				argv[i] == std::string("--help")
			) {
				display_usage(std::cout, argv[0]);
				std::cout << "internal commands:" << std::endl;
				std::cout << " /clear  clear screen" << std::endl;
				std::cout << " /exit   exit the program" << std::endl;
				std::cout << " /list   display command list" << std::endl;
				std::cout << " /skip   skip current prompt" << std::endl;
				std::cout << " /tell   give the answer" << std::endl;
				std::cout << " /tree   unfold the current node" << std::endl;
				return EXIT_SUCCESS;
			}
			else if (
				argv[i] == std::string("-v") or
				argv[i] == std::string("--version")
			) {
				std::cout << "imvoca version 1.0.4" << std::endl;
				return EXIT_SUCCESS;
			}
			else if (
				argv[i] == std::string("-s") or
				argv[i] == std::string("--search")
			) {
				if (++i < (unsigned int)argc) {
					mode = MODE_SEARCH;
					entry = argv[i];
				}
				else {
					std::cerr << "missing entry for \'" << argv[i - 1]
					<< "\' option" << std::endl;
					display_usage(std::cerr, argv[0]);
					return EXIT_FAILURE;
				}
			}
			else {
				std::cerr << "invalid parameter: " << argv[i] << std::endl;
				display_usage(std::cerr, argv[0]);
				return EXIT_FAILURE;
			}
		}
		else {
			if (fileName != nullptr) {
				std::cerr << "a single file name is expected" << std::endl;
				display_usage(std::cerr, argv[0]);
				return EXIT_FAILURE;
			}
			else {
				fileName = argv[i];
			}
		}
	}

	if (fileName == nullptr) {
		std::cerr << "missing file name" << std::endl;
		display_usage(std::cerr, argv[0]);
		return EXIT_FAILURE;
	}

	Json tree;
	try {
		std::ifstream file(fileName);
		if (not file.is_open()) {
			std::cerr << "unable to open file" << std::endl;
			return EXIT_FAILURE;
		}

		tree.parse(file);
	}
	catch (Json::bad_syntax& e) {
		std::cerr << "unable to parse json file due to bad syntax" << std::endl;
		return EXIT_FAILURE;
	}

	struct Data data = {
		true,
		EXIT_SUCCESS,
		std::cout,
		std::cin,
		0,
		"    "
	};

	switch (mode) {
		case MODE_NORMAL:
		recursive_explore(tree, data);
		break;
		case MODE_SEARCH:
		search_entry(tree, entry, data);
	}
	return data.rcode;
}
