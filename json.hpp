#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <map>

/* remove the following define to switch Object from std::vector to std::map */
#define SETTING_JSON_OBJECT_AS_VECTOR
/* std::vector will preserve order and duplicated properties */

class Json {
public:
	static Json Null();

	typedef std::string      String;
	typedef std::vector<Json> Array;
	typedef long int         Number;
	typedef bool            Boolean;

	#ifdef SETTING_JSON_OBJECT_AS_VECTOR
	typedef std::vector<std::pair<String, Json>> Object;
	#else
	typedef std::map             <String, Json>  Object;
	#endif

	enum Type {
		TYPE_OBJECT,
		TYPE_ARRAY,
		TYPE_STRING,
		TYPE_NUMBER,
		TYPE_BOOLEAN,
		TYPE_NULL
	};

	class bad_syntax: std::exception {};
	class bad_type  : std::exception {};

private:
	enum Type m_type;
	union {
		Object * object;
		Array  *  array;
		String * string;
		Number   number;
		Boolean boolean;
	} m_value;

public:
	Json(const Object& value);
	Json(const Array & value);
	Json(const String& value);
	Json(      Number  value);
	Json(      Boolean value);
	Json(                   );

	Json(std::istream& istream);
	Json           (const Json& src);
	Json& operator=(const Json& src);
	~Json();
	void clear();

	/* test type of json value */
	enum Type  type() const;
	bool is_object () const;
	bool is_array  () const;
	bool is_string () const;
	bool is_number () const;
	bool is_boolean() const;
	bool is_null   () const;

	/* access json value as c++ variables */
	      Object  &  object()      ;
	const Object  &  object() const;
	      Array   &   array()      ;
	const Array   &   array() const;
	      String  &  string()      ;
	const String  &  string() const;
	      Number  &  number()      ;
	const Number  &  number() const;
	      Boolean & boolean()      ;
	const Boolean & boolean() const;


	/* parses json code from istream into the json class */
	void parse(std::istream& istream);
	/* prints json code to ostream from the json class */
	void print(
		std::ostream& ostream,
		bool pretty = false,
		unsigned int indent = 0
	) const;


	/* individual parsers */
	/* directly converts json code to c++ variable */
	static void parse_object (std::istream& istream, Object&   object);
	static void parse_array  (std::istream& istream, Array&     array);
	static void parse_string (std::istream& istream, String&   string);
	static void parse_number (std::istream& istream, Number&   number);
	static void parse_boolean(std::istream& istream, Boolean& boolean);
	static void parse_null   (std::istream& istream                  );

	/* individual printers */
	/* directly converts c++ variable to json code */
	static void print_object(
		std::ostream& ostream,
		const Object& object,
		bool pretty = false,
		unsigned int indent = 0
	);
	static void print_array(
		std::ostream& ostream,
		const Array& array,
		bool pretty = false,
		unsigned int indent = 0
	);
	static void print_string(
		std::ostream& ostream,
		const String& string
	);
	static void print_number(
		std::ostream& ostream,
		const Number& number
	);
	static void print_boolean(
		std::ostream& ostream,
		const Boolean& boolean
	);
	static void print_null(
		std::ostream& ostream
	);

private:
	static void discard_space(std::istream& istream        );
	static void seek         (std::istream& istream, char c);
	static void print_indent (std::ostream& ostream, unsigned int level, const char* chars = "    ");
};

std::istream& operator>>(std::istream& istream,       Json& dst);
std::ostream& operator<<(std::ostream& ostream, const Json& src);

