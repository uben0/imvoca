# imvoca
simple program to revise your lessons and vocabulary

```
usage: ./imvoca [options] <dictionary.json>
options:
 -h, --help          display help
 -s, --search ENTRY  search for a specific entry
 -v, --version       display version
internal commands:
 /clear  clear screen
 /exit   exit the program
 /list   display command list
 /skip   skip current prompt
 /tell   give the answer
 /tree   unfold the current node
```
