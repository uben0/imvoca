BIN = imvoca
HPP = $(wildcard *.hpp)
CPP = $(wildcard *.cpp)
OBJ = $(patsubst %.cpp, %.o, $(CPP))
FLG = -Wall -Wextra -Werror -fmax-errors=1

$(BIN): $(OBJ)
	g++ $^ -o $@

%.o: %.cpp $(HPP) Makefile
	g++ $(FLG) -c $< -o $@

clean:
	rm -f $(OBJ) $(BIN)
